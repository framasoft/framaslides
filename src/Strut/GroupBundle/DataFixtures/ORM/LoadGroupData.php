<?php

namespace Strut\GroupBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Strut\GroupBundle\Entity\Group;
use Strut\StrutBundle\Entity\Presentation;
use Strut\StrutBundle\Entity\Version;
use Strut\UserBundle\Entity\User;

class LoadPresentationData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function load(ObjectManager $manager)
	{
		/** @var User $admin */
		$bob = $this->getReference('bob-user');

		$group = new Group("bob's group");

		$manager->persist($group);

		$bob->addAGroup($group, Group::ROLE_ADMIN);

		$manager->persist($bob);

		$manager->flush();
	}

	/**
	 * {@inheritdoc}
	 */
	public function getOrder()
	{
		return 40;
	}
}
